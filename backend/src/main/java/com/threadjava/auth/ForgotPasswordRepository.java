package com.threadjava.auth;

import com.threadjava.auth.model.ForgotPasswordUser;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;
import java.util.UUID;

public interface ForgotPasswordRepository extends CrudRepository<ForgotPasswordUser, String> {

    Optional<ForgotPasswordUser> findByToken(UUID token);

}

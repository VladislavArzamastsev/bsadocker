package com.threadjava.auth.validation;

import com.threadjava.auth.model.ChangePasswordDto;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import java.util.Objects;

@Component
public class ChangePasswordDtoValidator implements Validator {
    @Override
    public boolean supports(Class<?> clazz) {
        return Objects.equals(clazz, ChangePasswordDto.class);
    }

    @Override
    public void validate(Object target, Errors errors) {
        ChangePasswordDto dto = (ChangePasswordDto) target;
        validatePassword(dto, errors);
        checkPasswordMatch(dto, errors);
    }

    private void validatePassword(ChangePasswordDto dto, Errors errors) {
        if(dto.getPassword() == null || dto.getPassword().isBlank()){
            errors.reject("forgot-password.validation.password.invalid", "Password is invalid");
        }
    }

    private void checkPasswordMatch(ChangePasswordDto dto, Errors errors) {
        if(! Objects.equals(dto.getPassword(), dto.getRepeatedPassword())){
            errors.reject("forgot-password.validation.passwords.dont-match",
                    "Passwords don't match");
        }
    }
}

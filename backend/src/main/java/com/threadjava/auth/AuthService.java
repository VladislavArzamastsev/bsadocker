package com.threadjava.auth;

import com.threadjava.auth.dto.UserRegisterDto;
import com.threadjava.auth.model.AuthUser;
import com.threadjava.auth.dto.AuthUserDTO;
import com.threadjava.auth.dto.UserLoginDTO;
import com.threadjava.auth.model.ForgotPasswordUser;
import com.threadjava.users.model.User;
import com.threadjava.users.UsersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Optional;
import java.util.UUID;

@Service
public class AuthService {

    private final PasswordEncoder bCryptPasswordEncoder;
    private final AuthenticationManager authenticationManager;
    private final TokenService tokenService;
    private final UsersService userDetailsService;
    private final ForgotPasswordRepository forgotPasswordRepository;

    @Autowired
    public AuthService(PasswordEncoder bCryptPasswordEncoder,
                       AuthenticationManager authenticationManager,
                       TokenService tokenService,
                       UsersService userDetailsService,
                       ForgotPasswordRepository forgotPasswordRepository) {
        this.bCryptPasswordEncoder = bCryptPasswordEncoder;
        this.authenticationManager = authenticationManager;
        this.tokenService = tokenService;
        this.userDetailsService = userDetailsService;
        this.forgotPasswordRepository = forgotPasswordRepository;
    }

    public AuthUserDTO register(UserRegisterDto userDto) throws Exception {
        User user = AuthUserMapper.MAPPER.userRegisterDtoToUser(userDto);
        var loginDTO = new UserLoginDTO(user.getEmail(), user.getPassword());
        user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
        userDetailsService.save(user);
        return login(loginDTO);
    }

    public AuthUserDTO login(UserLoginDTO user) throws Exception {
        Authentication auth;
        try {
            auth = authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(user.getEmail(), user.getPassword()));
        }
        catch (BadCredentialsException e) {
            throw new Exception("Incorrect username or password", e);
        }

        var currentUser = (AuthUser)auth.getPrincipal();
        final var userDetails = userDetailsService.getUserById(currentUser.getId());
        final String jwt = tokenService.generateToken(currentUser);
        return new AuthUserDTO(jwt, userDetails);
    }

    public ForgotPasswordUser registerUserWhoForgotPassword(String email){
        ForgotPasswordUser user = new ForgotPasswordUser();
        user.setEmail(email);
        return forgotPasswordRepository.save(user);
    }

    public Optional<ForgotPasswordUser> findForgotUserByToken(UUID token){
        return forgotPasswordRepository.findByToken(token);
    }

    public void deleteForgotUserByEmail(String email){
        forgotPasswordRepository.deleteById(email);
    }

    public void updatePassword(String email, String password){
        userDetailsService.updatePassword(email, bCryptPasswordEncoder.encode(password));
    }
}
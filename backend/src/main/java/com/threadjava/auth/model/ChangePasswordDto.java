package com.threadjava.auth.model;

import lombok.Data;

@Data
public class ChangePasswordDto {

    private String password;
    private String repeatedPassword;

}

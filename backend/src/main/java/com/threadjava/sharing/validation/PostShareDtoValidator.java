package com.threadjava.sharing.validation;

import com.threadjava.sharing.dto.PostShareDto;
import org.apache.commons.validator.routines.EmailValidator;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import java.util.Objects;

@Component
public class PostShareDtoValidator implements Validator {

    @Override
    public boolean supports(Class<?> clazz) {
        return Objects.equals(clazz, PostShareDto.class);
    }

    @Override
    public void validate(Object target, Errors errors) {
        PostShareDto dto = (PostShareDto) target;
        validateToEmail(dto, errors);
        validateLink(dto, errors);
    }

    private void validateToEmail(PostShareDto dto, Errors errors){
        String email = dto.getToEmail();
        if(email == null){
            errors.reject("post-share.validation.email.not-present",
                    "Email is required");
        }else if(!EmailValidator.getInstance().isValid(email)){
            errors.reject("post-share.validation.email.invalid",
                    "Email is invalid");
        }
    }

    private void validateLink(PostShareDto dto, Errors errors){
        String link = dto.getLink();
        if(link == null || link.isBlank()){
            errors.reject("post-share.validation.link.not-present",
                    "Link is required");
        }
    }
}

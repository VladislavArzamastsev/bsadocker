package com.threadjava.sharing;

import com.threadjava.notification.EmailTextNotificator;
import com.threadjava.sharing.dto.PostShareDto;
import com.threadjava.sharing.validation.PostShareDtoValidator;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.mail.MailSendException;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/share/post")
public class PostShareController {

    private final PostShareDtoValidator postShareDtoValidator;
    private final EmailTextNotificator emailTextNotificator;

    public PostShareController(PostShareDtoValidator postShareDtoValidator,
                               EmailTextNotificator emailTextNotificator) {
        this.postShareDtoValidator = postShareDtoValidator;
        this.emailTextNotificator = emailTextNotificator;
    }

    @InitBinder
    public void initBinder(WebDataBinder webDataBinder) {
        webDataBinder.addValidators(postShareDtoValidator);
    }

    @PostMapping
    public ResponseEntity<Void> sharePostByEmail(@Validated @RequestBody PostShareDto dto,
                                                 BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        try {
            emailTextNotificator.sendEmail(dto.getToEmail(),
                    "Check out this post!",
                    "You might find this interesting: ".concat(dto.getLink()));
        } catch (MailSendException e) {
            e.printStackTrace();
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(HttpStatus.OK);
    }
}

package com.threadjava.commentReactions;

import com.threadjava.commentReactions.dto.ReceivedCommentReactionDto;
import com.threadjava.commentReactions.dto.ResponseCommentReactionDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Optional;
import static com.threadjava.auth.TokenService.getUserId;


@RestController
@RequestMapping("/api/commentreaction")
public class CommentReactionsController {

    private final CommentReactionService commentReactionService;
    private final SimpMessagingTemplate template;

    @Autowired
    public CommentReactionsController(CommentReactionService commentReactionService, SimpMessagingTemplate template) {
        this.commentReactionService = commentReactionService;
        this.template = template;
    }

    @PutMapping
    public Optional<ResponseCommentReactionDto> postReaction(@RequestBody ReceivedCommentReactionDto receivedCommentReactionDto){
        receivedCommentReactionDto.setUserId(getUserId());
        Optional<ResponseCommentReactionDto> out = commentReactionService.setReaction(receivedCommentReactionDto);
//        if (out.isPresent() && out.get().getUserId() != getUserId()) {
//            // notify a user if someone (not himself) liked his c
//            template.convertAndSend("/topic/like", "Your comment was liked!");
//        }
        return out;
    }

}

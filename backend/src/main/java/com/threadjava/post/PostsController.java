package com.threadjava.post;

import com.threadjava.post.dto.*;
import com.threadjava.post.filter.PostFilter;
import com.threadjava.post.validation.PostUpdateDtoValidator;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

import static com.threadjava.auth.TokenService.getUserId;

@RestController
@RequestMapping("/api/posts")
public class PostsController {

    private final PostsService postsService;
    private final SimpMessagingTemplate template;
    private final PostUpdateDtoValidator updateDtoValidator;

    public PostsController(PostsService postsService, SimpMessagingTemplate template, PostUpdateDtoValidator updateDtoValidator) {
        this.postsService = postsService;
        this.template = template;
        this.updateDtoValidator = updateDtoValidator;
    }

    @InitBinder("postUpdateDto")
    public void initBinder(WebDataBinder webDataBinder) {
        webDataBinder.addValidators(updateDtoValidator);
    }

    @GetMapping
    public List<PostListDto> get(@RequestParam(defaultValue = "0") Integer from,
                                 @RequestParam(defaultValue = "10") Integer count,
                                 @RequestParam(required = false) UUID userId,
                                 @RequestParam(required = false) Boolean showUserAndNotOthers,
                                 @RequestParam(required = false) Boolean showPostsThatWereLiked) {
        PostFilter postFilter = new PostFilter(userId, Boolean.FALSE, showUserAndNotOthers, showPostsThatWereLiked);
        return postsService.getPostsWithFilter(from, count, postFilter);
    }

    @GetMapping("/{id}")
    public PostDetailsDto get(@PathVariable UUID id) {
        return postsService.getPostById(id);
    }

    @PostMapping
    public PostCreationResponseDto post(@RequestBody PostCreationDto postDto) {
        postDto.setUserId(getUserId());
        var item = postsService.create(postDto);
        template.convertAndSend("/topic/new_post", item);
        return item;
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable("id") UUID id) {
        postsService.softDeletePost(id, getUserId());
    }

    @PutMapping("/{id}")
    public ResponseEntity<PostUpdateDto> update(@PathVariable("id") UUID id,
                                       @Validated @RequestBody PostUpdateDto postUpdateDto,
                                       BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
        }
        postUpdateDto.setUserId(getUserId());
        postsService.updatePost(id, postUpdateDto);
        return new ResponseEntity<>(postUpdateDto, HttpStatus.OK);
    }
}

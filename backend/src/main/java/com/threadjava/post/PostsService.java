package com.threadjava.post;

import com.threadjava.comment.CommentRepository;
import com.threadjava.post.dto.*;
import com.threadjava.post.filter.PostFilter;
import com.threadjava.post.model.Post;
import com.threadjava.users.UsersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class PostsService {

    private final PostsRepository postsCrudRepository;
    private final CommentRepository commentRepository;
    private final UsersService usersService;

    @Autowired
    public PostsService(PostsRepository postsCrudRepository, CommentRepository commentRepository, UsersService usersService) {
        this.postsCrudRepository = postsCrudRepository;
        this.commentRepository = commentRepository;
        this.usersService = usersService;
    }

    public void softDeletePost(UUID postId, UUID userId){
        postsCrudRepository.softDeleteById(postId, userId);
    }

    public List<PostListDto> getPostsWithFilter(Integer from, Integer count, PostFilter postFilter){

        PageRequest pageRequest = PageRequest.of(from / count, count);
        List<PostListQueryResult> results;
        if(postFilter.getUserId() == null){
            results = postsCrudRepository.findAllPostsWithoutUserId(postFilter, pageRequest);
        }else{
            results = postsCrudRepository.findAllPosts(postFilter, pageRequest);
        }
        return results
                .stream()
                .map(PostMapper.MAPPER::postListToPostListDto)
                .peek(postListDto -> {
                    postListDto.setUsersWhoLikedPost(usersService.getAllByReactionOnPost(postListDto.getId(), true));
                    postListDto.setUsersWhoDislikedPost(usersService.getAllByReactionOnPost(postListDto.getId(), false));
                })
                .collect(Collectors.toList());
    }

    public PostDetailsDto getPostById(UUID id) {
        PostDetailsDto post = postsCrudRepository.findPostById(id)
                .map(PostMapper.MAPPER::postToPostDetailsDto)
                .orElseThrow();
        post.setUsersWhoLikedPost(usersService.getAllByReactionOnPost(post.getId(), true));

        List<PostCommentDto> comments = commentRepository.findAllByPostIdAndIsDeleted(id, Boolean.FALSE)
                .stream()
                .map(commentDetailsQueryResult -> {
                    PostCommentDto postCommentDto = PostMapper.MAPPER
                            .commentDetailsQueryResultToPostCommentDto(commentDetailsQueryResult);
                    postCommentDto.setUsersWhoLikedComment(usersService
                            .getAllByReactionOnComment(commentDetailsQueryResult.getId(), true));
                    postCommentDto.setUsersWhoDislikedComment(usersService
                            .getAllByReactionOnComment(commentDetailsQueryResult.getId(), false));
                    return postCommentDto;
                })
                .collect(Collectors.toList());
        post.setComments(comments);

        return post;
    }

    public PostCreationResponseDto create(PostCreationDto postDto) {
        Post post = PostMapper.MAPPER.postDetailsDtoToPost(postDto);
        Post postCreated = postsCrudRepository.save(post);
        return PostMapper.MAPPER.postToPostCreationResponseDto(postCreated);
    }

    public Post getOne(UUID uuid){
        Post post = postsCrudRepository.getOne(uuid);
        post.setUsersWhoLikedPost(usersService.getAllByReactionOnPost(uuid, true));
        return post;
    }

    public void updatePost(UUID id, PostUpdateDto dto){
        Post post = PostMapper.MAPPER.postUpdateDtoToPost(dto);
        postsCrudRepository.update(id, post);
    }
}

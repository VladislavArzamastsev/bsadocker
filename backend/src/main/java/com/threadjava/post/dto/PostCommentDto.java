package com.threadjava.post.dto;

import com.threadjava.users.dto.UserDetailsDto;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Data
public class PostCommentDto {
    private UUID id;
    private String body;
    private PostUserDto user;
    private long likeCount;
    private long dislikeCount;
    private List<UserDetailsDto> usersWhoLikedComment = new ArrayList<>();
    private List<UserDetailsDto> usersWhoDislikedComment = new ArrayList<>();
}

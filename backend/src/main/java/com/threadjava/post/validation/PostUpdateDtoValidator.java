package com.threadjava.post.validation;

import com.threadjava.post.dto.PostUpdateDto;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import java.util.Objects;

@Component
public class PostUpdateDtoValidator implements Validator {
    @Override
    public boolean supports(Class<?> clazz) {
        return Objects.equals(clazz, PostUpdateDto.class);
    }

    @Override
    public void validate(Object target, Errors errors) {
        PostUpdateDto dto = (PostUpdateDto) target;
        if((dto.getBody() == null || dto.getBody().isBlank()) && dto.getImageId() == null){
            errors.reject("post.validation.blank-post",
                    "Post is blank");
        }
    }
}

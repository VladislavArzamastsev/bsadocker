package com.threadjava.post;

import com.threadjava.post.dto.PostDetailsQueryResult;
import com.threadjava.post.dto.PostListQueryResult;
import com.threadjava.post.filter.PostFilter;
import com.threadjava.post.model.Post;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface PostsRepository extends JpaRepository<Post, UUID> {

    @Query("UPDATE Post p SET p.isDeleted = true " +
            "WHERE p.id = :id AND p.user.id = :userId")
    @Modifying
    @Transactional
    void softDeleteById(@Param("id") UUID id, @Param("userId") UUID userId);

    @Query("UPDATE Post p SET " +
            "p.body = :#{#updatedData.body}, " +
            "p.image = :#{#updatedData.image} " +
            "WHERE p.id = :id AND p.user.id = :#{#updatedData.user.id}")
    @Modifying
    @Transactional
    void update(@Param("id") UUID id, @Param("updatedData") Post updatedData);

    @Query("SELECT new com.threadjava.post.dto.PostListQueryResult(p.id, p.body, " +
            "(SELECT COALESCE(SUM(CASE WHEN pr.isLike = TRUE THEN 1 ELSE 0 END), 0) FROM p.reactions pr WHERE pr.post = p), " +
            "(SELECT COALESCE(SUM(CASE WHEN pr.isLike = FALSE THEN 1 ELSE 0 END), 0) FROM p.reactions pr WHERE pr.post = p), " +
            "(SELECT COUNT(*) FROM p.comments c WHERE c.isDeleted = false), " +
            "p.createdAt, i, p.user) " +
            "FROM Post p " +
            "LEFT JOIN p.image i " +
            "WHERE (:#{#postFilter.isDeleted} IS NULL OR p.isDeleted = :#{#postFilter.isDeleted}) " +
            "ORDER BY p.createdAt DESC")
    List<PostListQueryResult> findAllPostsWithoutUserId(@Param("postFilter") PostFilter postFilter,
                                                     Pageable pageable);

    @Query("SELECT new com.threadjava.post.dto.PostListQueryResult(p.id, p.body, " +
            "(SELECT COALESCE(SUM(CASE WHEN pr.isLike = TRUE THEN 1 ELSE 0 END), 0) FROM p.reactions pr WHERE pr.post = p), " +
            "(SELECT COALESCE(SUM(CASE WHEN pr.isLike = FALSE THEN 1 ELSE 0 END), 0) FROM p.reactions pr WHERE pr.post = p), " +
            "(SELECT COUNT(*) FROM p.comments c WHERE c.isDeleted = false), " +
            "p.createdAt, i, p.user) " +
            "FROM Post p " +
            "LEFT JOIN p.image i " +
            "WHERE (" +
            "(:#{#postFilter.isDeleted} IS NULL OR p.isDeleted = :#{#postFilter.isDeleted}) " +
            "AND (:#{#postFilter.showUserAndNotOthers} IS NULL " +
            "      OR (:#{#postFilter.showUserAndNotOthers} = TRUE AND (p.user.id = :#{#postFilter.userId})) " +
            "      OR (:#{#postFilter.showUserAndNotOthers} = FALSE AND (p.user.id <> :#{#postFilter.userId})) " +
            "     )" +
            "AND (:#{#postFilter.showPostsThatWereLiked} IS NULL " +
            "       OR (SELECT pr.isLike FROM p.reactions pr " +
            "           WHERE pr.post = p AND pr.user.id = :#{#postFilter.userId}) = :#{#postFilter.showPostsThatWereLiked}" +
            "   ) " +
            ")" +
            "ORDER BY p.createdAt DESC")
    List<PostListQueryResult> findAllPosts(@Param("postFilter") PostFilter postFilter,
                                           Pageable pageable);

    @Query("SELECT new com.threadjava.post.dto.PostDetailsQueryResult(p.id, p.body, " +
            "(SELECT COALESCE(SUM(CASE WHEN pr.isLike = TRUE THEN 1 ELSE 0 END), 0) FROM p.reactions pr WHERE pr.post = p), " +
            "(SELECT COALESCE(SUM(CASE WHEN pr.isLike = FALSE THEN 1 ELSE 0 END), 0) FROM p.reactions pr WHERE pr.post = p), " +
            "(SELECT COUNT(*) FROM p.comments c WHERE c.isDeleted = false), " +
            "p.createdAt, p.updatedAt, i, p.user) " +
            "FROM Post p " +
            "LEFT JOIN p.image i " +
            "WHERE p.id = :id")
    Optional<PostDetailsQueryResult> findPostById(@Param("id") UUID id);
}
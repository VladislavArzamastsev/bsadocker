package com.threadjava.comment.dto;

import com.threadjava.users.model.User;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;
import java.util.UUID;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class CommentDetailsQueryResult {
    private UUID id;
    private String body;
    private User user;
    private UUID postId;
    private long likeCount;
    private long dislikeCount;
    private Date createdAt;
    private Date updatedAt;
}

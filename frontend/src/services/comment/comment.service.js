import { HttpMethod, ContentType } from 'src/common/enums/enums';

class Comment {
  constructor({ http }) {
    this._http = http;
  }

  getComment(id) {
    return this._http.load(`/api/comments/${id}`, {
      method: HttpMethod.GET
    });
  }

  addComment(payload) {
    return this._http.load('/api/comments', {
      method: HttpMethod.POST,
      contentType: ContentType.JSON,
      payload: JSON.stringify(payload)
    });
  }

  updateComment(id, updatedComment) {
    return this._http.load(`/api/comments/${id}`, {
      method: HttpMethod.PUT,
      contentType: ContentType.JSON,
      payload: JSON.stringify({
        body: updatedComment.body,
        postId: updatedComment.postId
      })
    });
  }

  deleteComment(id) {
    this._http.load(`/api/comments/${id}`, {
      method: HttpMethod.DELETE
    });
  }

  #addCommentReaction(commentId, isLikeValue) {
    return this._http.load('/api/commentreaction', {
      method: HttpMethod.PUT,
      contentType: ContentType.JSON,
      payload: JSON.stringify({
        commentId,
        isLike: isLikeValue
      })
    });
  }

  likeComment(commentId) {
    return this.#addCommentReaction(commentId, true);
  }

  dislikeComment(commentId) {
    return this.#addCommentReaction(commentId, false);
  }
}

export { Comment };

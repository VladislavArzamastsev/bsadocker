import * as React from 'react';
import validator from 'validator';

import { useDispatch } from 'react-redux';
import { profileActionCreator } from 'src/store/actions';
import PropTypes from 'prop-types';
import { IconName } from 'src/common/enums/components/icon-name.enum';
import { image as imageService } from 'src/services/services';
import { Button, Form, Image, Modal } from 'src/components/common/common';
import styles from './styles.module.scss';

const UpdateProfile = ({ user, close }) => {
  const updatedUser = { ...user };
  const [image, setImage] = React.useState(updatedUser.image);
  const [isUploading, setIsUploading] = React.useState(false);
  const [username, setUsername] = React.useState(updatedUser.username);
  const [status, setStatus] = React.useState(updatedUser.status);
  const [email, setEmail] = React.useState(updatedUser.email);
  const [isEmailValid, setIsEmailValid] = React.useState(true);
  const [isUsernameValid, setIsUsernameValid] = React.useState(true);
  const dispatch = useDispatch();

  const onProfileUpdate = React.useCallback(userToUpdate => {
    dispatch(profileActionCreator.updateUserProfile(userToUpdate));
  }, [dispatch]);

  const uploadImage = file => imageService.uploadImage(file);

  const handleUploadFile = async ({ target }) => {
    setIsUploading(true);
    const [file] = target.files;

    const img = await uploadImage(file);
    setImage(img);
    setIsUploading(false);
  };

  const emailChanged = data => {
    setEmail(data);
    setIsEmailValid(true);
  };

  const usernameChanged = data => {
    setUsername(data);
    setIsUsernameValid(true);
  };

  const statusChanged = data => {
    if (data === undefined) {
      return;
    }
    if (data === '') {
      setStatus(undefined);
    } else if (data.length <= 255) {
      setStatus(data);
    }
  };

  const handleEditProfile = async () => {
    const isValid = isEmailValid && isUsernameValid;
    if (!isValid || isUploading) {
      return;
    }
    if (user.username !== username || user.status !== status
        || user.email !== email || user.image !== image) {
      updatedUser.username = username;
      updatedUser.status = status;
      updatedUser.email = email;
      updatedUser.image = image;
      await onProfileUpdate(updatedUser);
    }
  };

  return (
    <Modal open onClose={close}>
      <Modal.Header className={styles.header}>
        <span>Edit Profile</span>
      </Modal.Header>
      <Modal.Content>
        <Form>
          {image && (
            <Image
              src={image.link}
              centered
              size="large"
            />
          )}
          <br />
          <div style={{ float: 'left' }}>
            <Button
              color="teal"
              isLoading={isUploading}
              isDisabled={isUploading}
              iconName={IconName.IMAGE}
            >
              <label className={styles.btnImgLabel}>
                Attach image
                <input
                  name="image"
                  type="file"
                  onChange={handleUploadFile}
                  hidden
                />
              </label>
            </Button>
          </div>
          <br />
          <br />
          <br />
          <Form.Input
            name="username"
            value={username}
            placeholder="Username"
            error={!isUsernameValid}
            onChange={ev => usernameChanged(ev.target.value)}
            onBlur={() => setIsUsernameValid(Boolean(username))}
          />
          <Form.TextArea
            name="status"
            value={status}
            placeholder="What do you think right now?"
            onChange={ev => statusChanged(ev.target.value)}
          />
          <Form.Input
            name="email"
            value={email}
            placeholder="Email"
            error={!isEmailValid}
            onChange={ev => emailChanged(ev.target.value)}
            onBlur={() => setIsEmailValid(validator.isEmail(email))}
          />
          <div>
            <div style={{ float: 'left' }}>
              <Button
                color="teal"
                isDisabled={isUploading}
                iconName={IconName.SUBMIT}
                onClick={handleEditProfile}
              >
                <label className={styles.btnImgLabel}>
                  Confirm
                </label>
              </Button>
            </div>
            <br />
          </div>
        </Form>
      </Modal.Content>
    </Modal>
  );
};

UpdateProfile.propTypes = {
  // eslint-disable-next-line react/forbid-prop-types
  user: PropTypes.object.isRequired,
  close: PropTypes.func.isRequired
};

export default UpdateProfile;

import PropTypes from 'prop-types';
import validator from 'validator';
import { useDispatch } from 'react-redux';
import * as React from 'react';
import { threadActionCreator } from 'src/store/actions';
import { Form } from 'semantic-ui-react';
import { Button, Modal } from 'src/components/common/common';
import { IconName } from 'src/common/enums/components/icon-name.enum';
import EmailWasSentMessage from 'src/components/common/email-was-sent-message/email-was-sent-message';
import styles from './styles.module.scss';

const ShareViaEmail = ({ linkToPost, close }) => {
  const [email, setEmail] = React.useState(undefined);
  const [showMessage, setShowMessage] = React.useState(false);
  const [isEmailValid, setIsEmailValid] = React.useState(true);
  const dispatch = useDispatch();

  const handlePostShare = React.useCallback((toEmail, link) => (
    dispatch(threadActionCreator.sharePost(toEmail, link))
  ), [dispatch]);

  const sendViaEmail = () => {
    if (email === undefined || email === '') {
      return;
    }
    if (validator.isEmail(email)) {
      handlePostShare(email, linkToPost);
    }
    setShowMessage(true);
  };

  const emailChanged = data => {
    setEmail(data === '' ? undefined : data);
  };

  return (
    <Modal open onClose={close}>
      <Modal.Header className={styles.header}>
        <span>Input email</span>
      </Modal.Header>
      <Modal.Content>
        <Form>
          <Form.Input
            name="email"
            value={email}
            placeholder="Email"
            error={!isEmailValid}
            onChange={ev => emailChanged(ev.target.value)}
            onBlur={() => setIsEmailValid(validator.isEmail(email))}
          />
          <div style={{ float: 'left' }}>
            <Button
              color="teal"
              iconName={IconName.SUBMIT}
              onClick={sendViaEmail}
            >
              <label className={styles.btnImgLabel}>
                Send
              </label>
            </Button>
            <br />
            <br />
          </div>
        </Form>
      </Modal.Content>
      {showMessage
      && (<EmailWasSentMessage close={() => setShowMessage(false)} email={email} />)}
    </Modal>
  );
};

ShareViaEmail.propTypes = {
  linkToPost: PropTypes.string.isRequired,
  close: PropTypes.func.isRequired
};

export default ShareViaEmail;

import * as React from 'react';
import { getFromNowTime } from 'src/helpers/helpers';
import { DEFAULT_USER_AVATAR } from 'src/common/constants/constants';
import { Comment as CommentUI, Icon, Label } from 'src/components/common/common';
import { commentType } from 'src/common/prop-types/prop-types';

import { IconName } from 'src/common/enums/components/icon-name.enum';
import PropTypes from 'prop-types';
import UserList from 'src/components/common/user-list/user-list';
import styles from './styles.module.scss';
import UpdateComment from './update-comment';

const Comment = ({
  comment,
  currentUserId,
  onCommentLike,
  onCommentDislike,
  onCommentUpdate,
  onCommentDelete }) => {
  const {
    id,
    body,
    createdAt,
    likeCount,
    dislikeCount,
    usersWhoLikedComment,
    usersWhoDislikedComment,
    user
  } = comment;

  const [editedComment, setEditedComment] = React.useState(undefined);
  const [showWhoLikedComment, setShowWhoLikedComment] = React.useState(false);
  const [showWhoDislikedComment, setShowWhoDislikedComment] = React.useState(false);
  const handleCommentLike = () => onCommentLike(id);
  const handleCommentDislike = () => onCommentDislike(id);
  const handleCommentDelete = () => onCommentDelete(id);

  return (
    <CommentUI className={styles.comment}>
      <CommentUI.Avatar src={user.image?.link ?? DEFAULT_USER_AVATAR} />
      <CommentUI.Content>
        <CommentUI.Author as="a">{user.username}</CommentUI.Author>
        <CommentUI.Metadata>{getFromNowTime(createdAt)}</CommentUI.Metadata>
        <CommentUI.Text>{body}</CommentUI.Text>
      </CommentUI.Content>
      <CommentUI.Content>
        <Label
          basic
          size="small"
          as="a"
          className={styles.toolbarBtn}
          onClick={handleCommentLike}
        >
          <Icon name={IconName.THUMBS_UP} />
          {likeCount}
        </Label>
        <Label
          basic
          size="small"
          as="a"
          className={styles.toolbarBtn}
          onClick={() => setShowWhoLikedComment(true)}
        >
          <Icon name={IconName.USERS_WHO_LIKED} />
          {likeCount}
        </Label>
        <Label basic size="small" as="a" className={styles.toolbarBtn} onClick={handleCommentDislike}>
          <Icon name={IconName.THUMBS_DOWN} />
          {dislikeCount}
        </Label>
        <Label
          basic
          size="small"
          as="a"
          className={styles.toolbarBtn}
          onClick={() => setShowWhoDislikedComment(true)}
        >
          <Icon name={IconName.USERS_WHO_DISLIKED} />
          {dislikeCount}
        </Label>
        {user.id === currentUserId
          && (
            <Label
              basic
              size="small"
              as="a"
              className={styles.toolbarBtn}
              onClick={() => setEditedComment(comment)}
            >
              <Icon name={IconName.EDIT} />
            </Label>
          )}
        {user.id === currentUserId
        && (
          <Label
            basic
            size="small"
            as="a"
            className={styles.toolbarBtn}
            onClick={() => handleCommentDelete(id)}
          >
            <Icon name={IconName.DELETE} />
          </Label>
        )}
        {editedComment && (
          <UpdateComment
            comment={editedComment}
            onCommentUpdate={onCommentUpdate}
            close={() => setEditedComment(undefined)}
          />
        )}
      </CommentUI.Content>
      {showWhoLikedComment && <UserList close={() => setShowWhoLikedComment(false)} users={usersWhoLikedComment} />}
      {showWhoDislikedComment
      && (<UserList close={() => setShowWhoDislikedComment(false)} users={usersWhoDislikedComment} />)}
    </CommentUI>
  );
};

Comment.propTypes = {
  comment: commentType.isRequired,
  currentUserId: PropTypes.string.isRequired,
  onCommentLike: PropTypes.func.isRequired,
  onCommentDislike: PropTypes.func.isRequired,
  onCommentUpdate: PropTypes.func.isRequired,
  onCommentDelete: PropTypes.func.isRequired
};

export default Comment;

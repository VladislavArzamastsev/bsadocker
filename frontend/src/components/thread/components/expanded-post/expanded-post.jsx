import * as React from 'react';
import PropTypes from 'prop-types';
import { useDispatch, useSelector } from 'react-redux';
import { threadActionCreator } from 'src/store/actions';
import { Spinner, Post, Modal, Comment as CommentUI } from 'src/components/common/common';
import AddComment from '../add-comment/add-comment';
import Comment from '../comment/comment';
import { getSortedComments } from './helpers/helpers';

const ExpandedPost = ({
  sharePost,
  handlePostLike,
  handlePostDislike,
  handlePostEdit,
  handlePostDelete
}) => {
  const dispatch = useDispatch();

  const { post } = useSelector(state => ({
    post: state.posts.expandedPost
  }));
  const { userId } = useSelector(state => ({
    userId: state.profile.user.id
  }));

  const handleCommentLike = React.useCallback(id => (
    dispatch(threadActionCreator.likeComment(id))
  ), [dispatch]);

  const handleCommentDislike = React.useCallback(id => (
    dispatch(threadActionCreator.dislikeComment(id))
  ), [dispatch]);

  const handleCommentUpdate = React.useCallback((id, updatedComment) => (
    dispatch(threadActionCreator.updateComment(id, updatedComment))
  ), [dispatch]);

  const handleCommentDelete = React.useCallback(id => (
    dispatch(threadActionCreator.deleteComment(id))
  ), [dispatch]);

  const handleCommentAdd = React.useCallback(commentPayload => (
    dispatch(threadActionCreator.addComment(commentPayload))
  ), [dispatch]);

  const handleExpandedPostToggle = React.useCallback(id => (
    dispatch(threadActionCreator.toggleExpandedPost(id))
  ), [dispatch]);

  const handleExpandedPostClose = () => handleExpandedPostToggle();

  const sortedComments = getSortedComments(post.comments ?? []);

  return (
    <Modal
      dimmer="blurring"
      centered={false}
      open
      onClose={handleExpandedPostClose}
    >
      {post ? (
        <Modal.Content>
          <Post
            post={post}
            onPostLike={handlePostLike}
            onPostDislike={handlePostDislike}
            onExpandedPostToggle={handleExpandedPostToggle}
            sharePost={sharePost}
            editPost={handlePostEdit}
            deletePost={handlePostDelete}
            currentUserId={userId}
          />
          <CommentUI.Group style={{ maxWidth: '100%' }}>
            <h3>Comments</h3>
            {sortedComments.map(comment => (
              <Comment
                comment={comment}
                currentUserId={userId}
                onCommentDelete={handleCommentDelete}
                onCommentLike={handleCommentLike}
                onCommentUpdate={handleCommentUpdate}
                onCommentDislike={handleCommentDislike}
                key={comment.id}
              />
            ))}
            <AddComment postId={post.id} onCommentAdd={handleCommentAdd} />
          </CommentUI.Group>
        </Modal.Content>
      ) : (
        <Spinner />
      )}
    </Modal>
  );
};

ExpandedPost.propTypes = {
  sharePost: PropTypes.func.isRequired,
  handlePostLike: PropTypes.func.isRequired,
  handlePostDislike: PropTypes.func.isRequired,
  handlePostEdit: PropTypes.func.isRequired,
  handlePostDelete: PropTypes.func.isRequired
};

export default ExpandedPost;

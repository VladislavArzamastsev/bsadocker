import * as React from 'react';
import PropTypes from 'prop-types';
import { getFromNowTime } from 'src/helpers/helpers';
import { IconName } from 'src/common/enums/enums';
import { postType } from 'src/common/prop-types/prop-types';
import { Icon, Card, Image, Label } from 'src/components/common/common';

import styles from './styles.module.scss';
import UserList from '../user-list/user-list';

const Post = ({ post,
  currentUserId,
  onPostLike,
  onPostDislike,
  onExpandedPostToggle,
  sharePost,
  editPost,
  deletePost }) => {
  const {
    id,
    image,
    body,
    user,
    likeCount,
    dislikeCount,
    commentCount,
    usersWhoLikedPost,
    usersWhoDislikedPost,
    createdAt
  } = post;
  const date = getFromNowTime(createdAt);

  const handlePostLike = () => onPostLike(id);
  const handlePostDislike = () => onPostDislike(id);
  const handleDeletePost = () => deletePost(id);
  const handleEditPost = () => editPost(id);
  const handleExpandedPostToggle = () => onExpandedPostToggle(id);

  const [showWhoLikedPost, setShowWhoLikedPost] = React.useState(false);
  const [showWhoDislikedPost, setShowWhoDislikedPost] = React.useState(false);

  return (
    <Card style={{ width: '100%' }}>
      {image && <Image src={image.link} wrapped ui={false} />}
      <Card.Content>
        <Card.Meta>
          <span className="date">
            posted by
            {' '}
            {user.username}
            {' - '}
            {date}
          </span>
        </Card.Meta>
        <Card.Description>{body}</Card.Description>
      </Card.Content>
      <Card.Content extra>
        <Label
          basic
          size="small"
          as="a"
          className={styles.toolbarBtn}
          onClick={handlePostLike}
        >
          <Icon name={IconName.THUMBS_UP} />
          {likeCount}
        </Label>
        <Label
          basic
          size="small"
          as="a"
          className={styles.toolbarBtn}
          onClick={() => setShowWhoLikedPost(true)}
        >
          <Icon name={IconName.USERS_WHO_LIKED} />
          {likeCount}
        </Label>
        <Label basic size="small" as="a" className={styles.toolbarBtn} onClick={handlePostDislike}>
          <Icon name={IconName.THUMBS_DOWN} />
          {dislikeCount}
        </Label>
        <Label
          basic
          size="small"
          as="a"
          className={styles.toolbarBtn}
          onClick={() => setShowWhoDislikedPost(true)}
        >
          <Icon name={IconName.USERS_WHO_DISLIKED} />
          {dislikeCount}
        </Label>
        <Label
          basic
          size="small"
          as="a"
          className={styles.toolbarBtn}
          onClick={handleExpandedPostToggle}
        >
          <Icon name={IconName.COMMENT} />
          {commentCount}
        </Label>
        <Label
          basic
          size="small"
          as="a"
          className={styles.toolbarBtn}
          onClick={() => sharePost(id)}
        >
          <Icon name={IconName.SHARE_ALTERNATE} />
        </Label>
        {user.id === currentUserId
        && (
          <Label
            basic
            size="small"
            as="a"
            className={styles.toolbarBtn}
            onClick={() => handleEditPost(id)}
          >
            <Icon name={IconName.EDIT} />
          </Label>
        )}
        {user.id === currentUserId
          && (
            <Label
              basic
              size="small"
              as="a"
              className={styles.toolbarBtn}
              onClick={() => handleDeletePost(id)}
            >
              <Icon name={IconName.DELETE} />
            </Label>
          )}
      </Card.Content>
      {showWhoLikedPost && <UserList close={() => setShowWhoLikedPost(false)} users={usersWhoLikedPost} />}
      {showWhoDislikedPost && <UserList close={() => setShowWhoDislikedPost(false)} users={usersWhoDislikedPost} />}
    </Card>
  );
};

Post.propTypes = {
  post: postType.isRequired,
  currentUserId: PropTypes.string.isRequired,
  onPostLike: PropTypes.func.isRequired,
  onPostDislike: PropTypes.func.isRequired,
  onExpandedPostToggle: PropTypes.func.isRequired,
  sharePost: PropTypes.func.isRequired,
  editPost: PropTypes.func.isRequired,
  deletePost: PropTypes.func.isRequired
};

export default Post;

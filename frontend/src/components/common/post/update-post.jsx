import PropTypes from 'prop-types';
import { image as imageService } from 'src/services/services';
import * as React from 'react';
import { Button, Form, Image, Modal } from 'src/components/common/common';
import styles from 'src/components/thread/components/shared-post-link/styles.module.scss';

import { useDispatch } from 'react-redux';
import { IconName } from 'src/common/enums/components/icon-name.enum';
import { threadActionCreator } from 'src/store/actions';

const UpdatePost = ({ post, close }) => {
  const updatedPost = { ...post };

  const [image, setImage] = React.useState(updatedPost.image);
  const [isUploading, setIsUploading] = React.useState(false);
  const [body, setBody] = React.useState(updatedPost.body);

  const uploadImage = file => imageService.uploadImage(file);
  const dispatch = useDispatch();

  const handleUploadFile = async ({ target }) => {
    setIsUploading(true);
    const [file] = target.files;

    const img = await uploadImage(file);
    setImage(img);
    setIsUploading(false);
  };

  const bodyChanged = data => {
    setBody(data === '' ? undefined : data);
  };

  const onPostUpdate = React.useCallback(postToUpdate => {
    dispatch(threadActionCreator.updatePost(postToUpdate.id, postToUpdate));
  }, [dispatch]);

  const handleEdit = () => {
    if (!(image || body)) {
      return;
    }
    if (post.body !== body || post.image !== image) {
      updatedPost.image = image;
      updatedPost.body = body;
      onPostUpdate(updatedPost);
    }
  };

  return (
    <Modal open onClose={close}>
      <Modal.Header className={styles.header}>
        <span>Edit Post</span>
      </Modal.Header>
      <Modal.Content>
        <Form>
          {image && (
            <Image
              src={image.link}
              centered
              size="large"
            />
          )}
          <br />
          <div style={{ float: 'left' }}>
            <Button
              color="teal"
              isLoading={isUploading}
              isDisabled={isUploading}
              iconName={IconName.IMAGE}
            >
              <label className={styles.btnImgLabel}>
                Attach image
                <input
                  name="image"
                  type="file"
                  onChange={handleUploadFile}
                  hidden
                />
              </label>
            </Button>
          </div>
          <br />
          <br />
          <br />
          <Form.TextArea
            name="body"
            value={body}
            placeholder="What's going on your life?"
            onChange={ev => bodyChanged(ev.target.value)}
          />
          <div>
            <div style={{ float: 'left' }}>
              <Button
                color="teal"
                isDisabled={isUploading}
                iconName={IconName.SUBMIT}
                onClick={handleEdit}
              >
                <label className={styles.btnImgLabel}>
                  Confirm
                </label>
              </Button>
            </div>
            <br />
          </div>
        </Form>
      </Modal.Content>
    </Modal>
  );
};

UpdatePost.propTypes = {
  // eslint-disable-next-line react/forbid-prop-types
  post: PropTypes.object.isRequired,
  close: PropTypes.func.isRequired
};

export default UpdatePost;

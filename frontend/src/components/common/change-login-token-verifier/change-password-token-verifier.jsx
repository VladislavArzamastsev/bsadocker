import * as React from 'react';
import PropTypes from 'prop-types';
import { profileActionCreator } from 'src/store/actions';
import { locationType } from 'src/common/prop-types/location';
import WrongTokenPage from './wrong-token-page';

const ChangePasswordTokenVerifier = ({ component: Component, ...rest }) => {
  const [verified, setVerified] = React.useState(false);
  React.useEffect(() => {
    const fetchVerified = async () => {
      const response = await profileActionCreator.verifyForgotPasswordToken(rest.location.search);
      const isVerified = await response;
      setVerified(isVerified);
    };
    fetchVerified();
  }, [rest.location.search, verified]);

  return (verified ? (
    <Component />
  ) : (
    <WrongTokenPage />
  )
  );
};

ChangePasswordTokenVerifier.propTypes = {
  component: PropTypes.elementType.isRequired,
  location: locationType
};

ChangePasswordTokenVerifier.defaultProps = {
  location: undefined
};

export default ChangePasswordTokenVerifier;

import * as React from 'react';
import { Modal } from 'src/components/common/common';
import PropTypes from 'prop-types';
import styles from './styles.module.scss';

const EmailWasSentMessage = ({ email, close }) => (
  <Modal open onClose={close} style={{ width: '40%' }}>
    <Modal.Header className={styles.header} style={{ fontSize: '2vw' }}>
      <span>Attention!</span>
    </Modal.Header>
    <Modal.Content>
      <div style={{ fontSize: '1.5vw' }}>
        {`An email was sent to: ${email}`}
      </div>
    </Modal.Content>
  </Modal>
);

EmailWasSentMessage.propTypes = {
  email: PropTypes.string.isRequired,
  close: PropTypes.func.isRequired
};

export default EmailWasSentMessage;

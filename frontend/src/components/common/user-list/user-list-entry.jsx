import * as React from 'react';
import PropTypes from 'prop-types';
import { Comment as CommentUI } from 'semantic-ui-react';
import { DEFAULT_USER_AVATAR } from 'src/common/constants/user.constants';
import styles from './styles.module.scss';

const UserListEntry = ({ user }) => (
  <CommentUI className={styles.comment}>
    <CommentUI.Avatar src={user.image?.link ?? DEFAULT_USER_AVATAR} />
    <CommentUI.Content>
      <CommentUI.Author as="a">{user.username}</CommentUI.Author>
    </CommentUI.Content>
  </CommentUI>
);

UserListEntry.propTypes = {
  // eslint-disable-next-line react/forbid-prop-types
  user: PropTypes.object.isRequired
};

export default UserListEntry;

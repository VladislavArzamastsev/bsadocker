import { createAction } from '@reduxjs/toolkit';
import {
  comment as commentService,
  auth,
  post as postService
} from 'src/services/services';

const ActionType = {
  ADD_POST: 'thread/add-post',
  LOAD_MORE_POSTS: 'thread/load-more-posts',
  SET_ALL_POSTS: 'thread/set-all-posts',
  SET_EXPANDED_POST: 'thread/set-expanded-post'
};

const setPosts = createAction(ActionType.SET_ALL_POSTS, posts => ({
  payload: {
    posts
  }
}));

const addMorePosts = createAction(ActionType.LOAD_MORE_POSTS, posts => ({
  payload: {
    posts
  }
}));

const addPost = createAction(ActionType.ADD_POST, post => ({
  payload: {
    post
  }
}));

const setExpandedPost = createAction(ActionType.SET_EXPANDED_POST, post => ({
  payload: {
    post
  }
}));

const loadPosts = filter => async dispatch => {
  const posts = await postService.getAllPosts(filter);
  dispatch(setPosts(posts));
};

const loadMorePosts = filter => async (dispatch, getRootState) => {
  const {
    posts: { posts }
  } = getRootState();
  const loadedPosts = await postService.getAllPosts(filter);
  const filteredPosts = loadedPosts.filter(
    post => !(posts && posts.some(loadedPost => post.id === loadedPost.id))
  );
  dispatch(addMorePosts(filteredPosts));
};

const applyPost = postId => async dispatch => {
  const post = await postService.getPost(postId);
  dispatch(addPost(post));
};

const createPost = post => async dispatch => {
  const { id } = await postService.addPost(post);
  const newPost = await postService.getPost(id);
  dispatch(addPost(newPost));
};

const toggleExpandedPost = postId => async dispatch => {
  const post = postId ? await postService.getPost(postId) : undefined;
  dispatch(setExpandedPost(post));
};

const likePost = postId => async (dispatch, getRootState) => {
  const response = await postService.likePost(postId);
  const user = await auth.getCurrentUser();
  const delta = response?.id ? 1 : -1; // if ID exists then the post was liked, otherwise - like was removed
  const mapLikes = post => ({
    ...post,
    likeCount: Number(post.likeCount) + delta,
    usersWhoLikedPost: delta > 0
      ? [...(post.usersWhoLikedPost || []), user]
      : [...(post.usersWhoLikedPost || [])].filter(u => u.id !== user.id),
    usersWhoDislikedPost: [...(post.usersWhoDislikedPost || [])].filter(u => u.id !== user.id)
  });

  const {
    posts: { posts, expandedPost }
  } = getRootState();
  const updated = posts.map(post => (post.id !== postId ? post : mapLikes(post)));
  dispatch(setPosts(updated));

  if (expandedPost && expandedPost.id === postId) {
    dispatch(setExpandedPost(mapLikes(expandedPost)));
  }
};

const dislikePost = postId => async (dispatch, getRootState) => {
  const response = await postService.dislikePost(postId);
  const user = await auth.getCurrentUser();
  const delta = response?.id ? 1 : -1; // if ID exists then the post was disliked, otherwise - dislike was removed
  const mapDislikes = post => ({
    ...post,
    dislikeCount: Number(post.dislikeCount) + delta,
    usersWhoDislikedPost: delta > 0
      ? [...(post.usersWhoDislikedPost || []), user]
      : [...(post.usersWhoDislikedPost || [])].filter(u => u.id !== user.id),
    usersWhoLikedPost: [...(post.usersWhoLikedPost || [])].filter(u => u.id !== user.id)
  });

  const {
    posts: { posts, expandedPost }
  } = getRootState();
  const updated = posts.map(post => (post.id !== postId ? post : mapDislikes(post)));

  dispatch(setPosts(updated));

  if (expandedPost && expandedPost.id === postId) {
    dispatch(setExpandedPost(mapDislikes(expandedPost)));
  }
};

const updatePost = (postId, updatedPost) => async (dispatch, getRootState) => {
  const updated = await postService.updatePost(postId, updatedPost);
  if (!updated) {
    return;
  }
  const {
    posts: { posts, expandedPost }
  } = getRootState();

  const mapPost = post => ({
    ...post,
    image: updatedPost.image,
    body: updatedPost.body
  });

  const updatedPosts = posts.map(post => (post.id === postId ? mapPost(post) : post));
  dispatch(setPosts(updatedPosts));

  if (expandedPost && expandedPost.id === postId) {
    dispatch(setExpandedPost(mapPost(expandedPost)));
  }
};

const deletePost = postId => async (dispatch, getRootState) => {
  await postService.deletePost(postId);
  const {
    posts: { posts }
  } = getRootState();
  const updated = posts.filter(post => postId !== post.id);
  dispatch(setPosts(updated));
};

const sharePost = (emailOfRecipient, linkToPost) => async () => {
  await postService.sharePost(emailOfRecipient, linkToPost);
};

const addComment = request => async (dispatch, getRootState) => {
  const { id } = await commentService.addComment(request);
  const comment = await commentService.getComment(id);

  const mapComments = post => ({
    ...post,
    commentCount: Number(post.commentCount) + 1,
    comments: [...(post.comments || []), comment] // comment is taken from the current closure
  });

  const {
    posts: { posts, expandedPost }
  } = getRootState();
  const updated = posts.map(post => (post.id !== comment.postId ? post : mapComments(post)));

  dispatch(setPosts(updated));

  if (expandedPost && expandedPost.id === comment.postId) {
    dispatch(setExpandedPost(mapComments(expandedPost)));
  }
};

const deleteComment = commentId => async (dispatch, getRootState) => {
  await commentService.deleteComment(commentId);
  const comment = await commentService.getComment(commentId);

  const {
    posts: { posts, expandedPost }
  } = getRootState();

  const mapComments = post => ({
    ...post,
    commentCount: Number(post.commentCount) - 1,
    comments: [...(post.comments || [])].filter(c => c.id !== commentId)
  });
  const updated = posts.map(post => (post.id !== comment.postId ? post : mapComments(post)));

  dispatch(setPosts(updated));

  if (expandedPost && expandedPost.id === comment.postId) {
    dispatch(setExpandedPost(mapComments(expandedPost)));
  }
};

const likeComment = commentId => async (dispatch, getRootState) => {
  const response = await commentService.likeComment(commentId);
  const user = await auth.getCurrentUser();
  const delta = response?.id ? 1 : -1;
  // if ID exists then the comment was liked, otherwise - like was removed
  const mapLikesForComment = comment => ({
    ...comment,
    likeCount: Number(comment.likeCount) + delta,
    usersWhoLikedComment: delta > 0
      ? [...(comment.usersWhoLikedComment || []), user]
      : [...(comment.usersWhoLikedComment || [])].filter(u => u.id !== user.id),
    usersWhoDislikedComment: [...(comment.usersWhoDislikedComment || [])].filter(u => u.id !== user.id)
  });

  const {
    posts: { expandedPost }
  } = getRootState();

  const mapForPost = post => ({
    ...post,
    comments: [...(post.comments || [])].map(c => (c.id === commentId ? mapLikesForComment(c) : c))
  });
  const comment = await commentService.getComment(commentId);
  if (expandedPost && expandedPost.id === comment.postId) {
    dispatch(setExpandedPost(mapForPost(expandedPost)));
  }
};

const dislikeComment = commentId => async (dispatch, getRootState) => {
  const response = await commentService.dislikeComment(commentId);
  const user = await auth.getCurrentUser();
  const delta = response?.id ? 1 : -1;
  // if ID exists then the comment was liked, otherwise - like was removed
  const mapDislikesForComment = comment => ({
    ...comment,
    dislikeCount: Number(comment.dislikeCount) + delta,
    usersWhoDislikedComment: delta > 0
      ? [...(comment.usersWhoDislikedComment || []), user]
      : [...(comment.usersWhoDislikedComment || [])].filter(u => u.id !== user.id),
    usersWhoLikedComment: [...(comment.usersWhoLikedComment || [])].filter(u => u.id !== user.id)
  });

  const {
    posts: { expandedPost }
  } = getRootState();

  const mapForPost = post => ({
    ...post,
    comments: [...(post.comments || [])].map(c => (c.id === commentId ? mapDislikesForComment(c) : c))
  });
  const comment = await commentService.getComment(commentId);
  if (expandedPost && expandedPost.id === comment.postId) {
    dispatch(setExpandedPost(mapForPost(expandedPost)));
  }
};

const updateComment = (commentId, updatedComment) => async (dispatch, getRootState) => {
  const response = await commentService.updateComment(commentId, updatedComment);
  if (!response) {
    return;
  }

  const mapBodyForComment = comment => ({
    ...comment,
    body: updatedComment.body
  });

  const mapComments = post => ({
    ...post,
    comments: [...(post.comments || [])].map(c => (c.id === commentId ? mapBodyForComment(c) : c))
  });

  const {
    posts: { expandedPost }
  } = getRootState();

  const comment = await commentService.getComment(commentId);
  if (expandedPost && expandedPost.id === comment.postId) {
    dispatch(setExpandedPost(mapComments(expandedPost)));
  }
};

export {
  setPosts,
  addMorePosts,
  addPost,
  setExpandedPost,
  loadPosts,
  loadMorePosts,
  applyPost,
  createPost,
  toggleExpandedPost,
  likePost,
  dislikePost,
  updatePost,
  deletePost,
  sharePost,
  deleteComment,
  updateComment,
  likeComment,
  dislikeComment,
  addComment
};

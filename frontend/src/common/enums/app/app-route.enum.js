const AppRoute = {
  ROOT: '/',
  ANY: '*',
  LOGIN: '/login',
  REGISTRATION: '/registration',
  PROFILE: '/profile',
  FORGOT_PASSWORD: '/forgot-password',
  SHARE_$POSTHASH: '/share/:postHash'
};

export { AppRoute };
